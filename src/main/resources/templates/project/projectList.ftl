<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>参赛项目列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">   
    <div class="layui-card">
      <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">项目名称：</label>
            <div class="layui-input-block">
              <input type="text" name="loginname" placeholder="请输入项目名称" autocomplete="off" class="layui-input">
            </div>
          </div>
          
          <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>
      
      <div class="layui-card-body">
        <table id="projectTable" lay-filter="projectTable"></table>
      </div>
    </div>
  </div>

 <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'project', 'table'], function(){
    var $ = layui.$
    ,form = layui.form
    ,table = layui.table;
    
    //监听搜索
    form.on('submit(search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('projectTable', {
        where: field
      });
    });
  
    //事件
    var active = {
      add: function(){
        location.href="addCompanyInfo";
      }
    }  
    $('.layui-btn.layuiadmin-btn-admin').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });
  }).use(['index','project', 'laydate'], function(){
    var laydate = layui.laydate;
    
    //开始日期
    var insStart = laydate.render({
      elem: '#test-laydate-start'
      ,type: 'datetime'
      ,done: function(value, date){
        //更新结束日期的最小日期
        insEnd.config.min = lay.extend({}, date, {
          month: date.month - 1
        });        
        //自动弹出结束日期的选择器
       insEnd.config.elem[0].focus();
      }
    });
    
    //结束日期
    var insEnd = laydate.render({
      elem: '#test-laydate-end'
      ,type: 'datetime'
    });
  });
  </script>
</body>
</html>

