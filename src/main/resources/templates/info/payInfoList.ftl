<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>缴费管理(省级)</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
		<link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
	</head>

	<body>

		<div class="layui-fluid">
			<div class="layui-card">
				<div class="layui-form layui-card-header layuiadmin-card-header-auto">
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">单位名称：</label>
							<div class="layui-input-block">
								<input type="text" name="loginname" placeholder="请输入单位名称" autocomplete="off" class="layui-input" style="width:212px">
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">所在地区：</label>
							<div class="layui-input-block">
								<select name="provinceId" lay-filter="provinceId" class="provinceId">
									<option value="">请选择所在省</option>
								</select>
							</div>
						</div>
						<div class="layui-inline">
							<select name="cityId" lay-filter="cityId" disabled>
								<option value="">请选择所在市</option>
							</select>
						</div>
						<div class="layui-inline">
							<select name="areaId" lay-filter="areaId" disabled>
								<option value="">请选择所在区</option>
							</select>
						</div>
						<div class="layui-inline" style="padding-left:40px">
						    <input type="radio" name="role" value="" title="全部" checked>
							<input type="radio" name="role" value="" title="学校" >
							<input type="radio" name="role" value="" title="机构">
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">缴费状态：</label>
							<div class="layui-input-block">
								<select name="role1">
									<option value="0">请选择</option>
									<option value="1">已缴费</option>
									<option value="2">未缴费</option>
								</select>
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">参赛项目：</label>
							<div class="layui-input-block">
								<input type="text" name="loginname" placeholder="请输入参赛项目" autocomplete="off" class="layui-input" style="width:212px">
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">组别：</label>
							<div class="layui-input-block">
								<select name="role1">
									<option value="0">请选择组别</option>
									<option value="1">小学</option>
									<option value="2">初中</option>
									<option value="3">高中</option>
								</select>
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">报名时间：</label>
							<div class="layui-input-inline">
								<input type="text" class="layui-input" id="test-laydate-start" placeholder="报名开始时间">
							</div>
							<div class="layui-form-mid">
								-
							</div>
							<div class="layui-input-inline">
								<input type="text" class="layui-input" id="test-laydate-end" placeholder="报名结束时间">
							</div>
						</div>

						<div class="layui-inline">
							<button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
						</div>
					</div>
				</div>

				<div class="layui-card-body">
					<table id="manage" lay-filter="manage"></table>
				</div>
			</div>
		</div>

		<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
		<script>
			layui.config({
				base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
			}).extend({
				index: 'lib/index', //主入口模块
				address : 'address'
			}).use(['index', 'payinfo', 'table', 'jquery', 'address'], function() {
				var $ = layui.$,
					form = layui.form,
					table = layui.table
					,address = layui.address();

				//监听搜索
				form.on('submit(search)', function(data) {
					var field = data.field;

					//执行重载
					table.reload('manage', {
						where: field
					});
				});

				//事件
				var active = {
					add: function() {
						location.href = "addCompanyInfo";
					}
				}
				$('.layui-btn.layuiadmin-btn-admin').on('click', function() {
					var type = $(this).data('type');
					active[type] ? active[type].call(this) : '';
				});
			}).use(['index', 'laydate'], function() {
				var laydate = layui.laydate;

				//开始日期
				var insStart = laydate.render({
					elem: '#test-laydate-start',
					type: 'datetime',
					done: function(value, date) {
						//更新结束日期的最小日期
						insEnd.config.min = lay.extend({}, date, {
							month: date.month - 1
						});
						//自动弹出结束日期的选择器
						insEnd.config.elem[0].focus();
					}
				});

				//结束日期
				var insEnd = laydate.render({
					elem: '#test-laydate-end',
					type: 'datetime'
				});
			});
		</script>
	</body>

</html>