

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>导出页</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-app-form-list" id="layuiadmin-app-form-list" style="padding: 20px 30px 0 0;">

    <div class="layui-form-item">
      <label class="layui-form-label">全选：</label>
      <div class="layui-input-inline">
        <input type="checkbox" name="cityId" lay-skin="switch" id="c_all"  lay-filter="switchTest" lay-text="ON|OFF">
      </div>
    </div>
	<div class="layui-form-item">
      <label class="layui-form-label">所在地区：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="cityId1" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">学校/机构：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="cityId2" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">单位名称：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="cityId3" lay-skin="switch" lay-text="ON|OFF" checked lay-filter="c_one" class="cityId">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">队伍名称：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" checked lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">缴费状态：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" checked lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">参赛项目小类：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">参赛项目大类：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">组别：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">辅导老师：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">成绩细节：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">总成绩：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">辅导老师1：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">辅导老师2：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">队员姓名：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">队员性别：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">队员身份证号：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
      <label class="layui-form-label">队员对付尺码：</label>
      <div class="layui-input-inline" style="width:60px">
        <input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" lay-filter="c_one" class="cityId">
      </div>
    </div>
  </div>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
  <script src="${request.contextPath}/js/jquery.min.js"></script>
  <script>
  	 layui.use('form', function () {
        var form = layui.form;
 
        //有一个未选中 取消全选
        form.on('switch(c_one)', function (data) {
            var item = $(".cityId");
            for (var i = 0; i < item.length; i++) {
                if (item[i].checked == false) {
                    $("#c_all").attr("checked", false);
                    form.render('checkbox');
                    break;
                }
            }
            //如果都勾选了  勾上全选
            var  all=item.length;
            for (var i = 0; i < item.length; i++) {
                if (item[i].checked == true) {
                    all--;
                }
            }
            if(all==0){
                $("#c_all").attr("checked", true);
                form.render('checkbox');}
        });
 
        //反选
        form.on('switch(switchTest)', function(data){
            if(this.checked){
                var a = data.elem.checked;
                if (a == true) {
                    $(".cityId").attr("checked", true);
                    form.render('checkbox');
                } else {
                    $(".cityId").attr("checked", false);
                    form.render('checkbox');
                }
            }else{
                var item = $(".cityId");
                item.each(function () {
                    if ($(this).attr("checked")) {
                        $(this).attr("checked", false);
                    } else {
                        $(this).attr("checked", true);
                    }
                })
                form.render('checkbox');
            }
 
        });
 
 
    });

  </script>
</body>
</html>