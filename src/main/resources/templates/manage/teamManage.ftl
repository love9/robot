

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>队员管理</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        <div class="layui-form-item">
          <div class="layui-inline">
				<label class="layui-form-label">单位名称：</label>
				<div class="layui-input-block">
					<input type="text" name="companyName" placeholder="请输入单位名称" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">所在地区：</label>
				<div class="layui-input-block">
					<select name="provinceId" lay-filter="provinceId" class="provinceId">
						<option value="">请选择所在省</option>
					</select>
				</div>
			</div>
			<div class="layui-inline">
				<select name="cityId" lay-filter="cityId" disabled>
					<option value="">请选择所在市</option>
				</select>
			</div>
			<div class="layui-inline">
				<select name="areaId" lay-filter="areaId" disabled>
					<option value="">请选择所在区</option>
				</select>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">单位性质：</label>
				<div class="layui-inline">
				    <input type="radio" name="companyType" value="1" title="全部" checked>
					<input type="radio" name="companyType" value="1" title="学校" >
					<input type="radio" name="companyType" value="2" title="机构">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">参赛项目：</label>
				<div class="layui-input-block">
					<input type="text" name="groupType" placeholder="请输入参赛项目" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">队伍名称：</label>
				<div class="layui-input-block">
					<input type="text" name="teamName" placeholder="请输入队伍名称" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">组别：</label>
				<div class="layui-input-block">
					<select name="groupType">
						<option value="">请选择组别</option>
						<option value="1">小学</option>
						<option value="2">初中</option>
						<option value="3">高中</option>
					</select>
				</div>
			</div>		
			<div class="layui-outline">
			<div class="layui-inline">
				<label class="layui-form-label">队员姓名：</label>
				<div class="layui-input-block">
					<input type="text" name="name" placeholder="请输入队员姓名" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>				
			
			<div class="layui-inline">
				<label class="layui-form-label">成绩总分：</label>
				<div class="layui-input-inline" style="width:20%">
					<input type="text" class="layui-input"  placeholder="请输入分数" />
				</div>
				<div class="layui-form-mid">分</div>
				<div class="layui-form-mid">
					-
				</div>
				<div class="layui-input-inline" style="width:20%">
					<input type="text" class="layui-input"  placeholder="请输入分数">
				</div>
				<div class="layui-form-mid">分</div>
			</div>
			</div>	
			<div class="layui-outline">				
			<div class="layui-inline">
				<label class="layui-form-label">队员身份证号：</label>
				<div class="layui-input-block">
					<input type="text" name="teamMemberIdCard" placeholder="请输入队员身份证号" autocomplete="off" class="layui-input" style="width:212px">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">报名时间：</label>
				<div class="layui-input-inline">
					<input type="text" class="layui-input" id="test-laydate-start" placeholder="报名开始时间">
				</div>
				<div class="layui-form-mid">
					-
				</div>
				<div class="layui-input-inline">
					<input type="text" class="layui-input" id="test-laydate-end" placeholder="报名结束时间">
				</div>
			</div>
          <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>

      <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
          <button class="layui-btn layuiadmin-btn-list" data-type="updateExcle">导出信息</button>
        </div>
        <table id="manage" lay-filter="manage"></table>
      </div>
    </div>
  </div>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index', //主入口模块
    address : 'address'
  }).use(['index', 'teammanage', 'table', 'jquery', 'address'], function(){
    var table = layui.table
    ,form = layui.form
    ,address = layui.address();
    
    //监听搜索
    form.on('submit(search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('manage', {
        where: field
      });
    });
    
    var $ = layui.$, active = {
      updateExcle: function(){
        layer.open({
          type: 2
          ,title: '选择需要导出的字段'
          ,content: ['listform', 'no']
          ,area: ['650px', '500px']
          ,btn: ['导出', '取消']
          ,yes: function(index, layero){
            //点击确认触发 iframe 内容中的按钮提交
            var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
            submit.click();
          }
        }); 
      }
    }; 

    $('.layui-btn.layuiadmin-btn-list').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });

  }).use(['index', 'laydate'], function() {
		var laydate = layui.laydate;

		//开始日期
		var insStart = laydate.render({
			elem: '#test-laydate-start',
			type: 'datetime',
			done: function(value, date) {
				//更新结束日期的最小日期
				insEnd.config.min = lay.extend({}, date, {
					month: date.month - 1
				});
				//自动弹出结束日期的选择器
				insEnd.config.elem[0].focus();
			}
		});

		//结束日期
		var insEnd = laydate.render({
			elem: '#test-laydate-end',
			type: 'datetime'
		});
	});
  </script>
</body>
</html>
