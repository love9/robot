package com.star.robot.enums;

import com.star.robot.entity.Project;

import javax.persistence.*;

public enum ProjectLevelEnum {
    PROVINCELEVEL(0,"省级别") , CITYLEVEL(1,"市级别");
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;



    private String name;


    ProjectLevelEnum(Integer id,String name){
        this.id = id;
        this.name = name;
    }

}
