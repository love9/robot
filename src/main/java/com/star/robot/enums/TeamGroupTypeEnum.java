package com.star.robot.enums;

public enum TeamGroupTypeEnum {
    XIAOXUE(0,"小学组") , CHUZHONG(1,"初中组"),GAOZHONG(2,"高中组");
    private Integer code;

    private String name;

    TeamGroupTypeEnum(Integer code ,String name){
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
