package com.star.robot.entity;

import com.star.robot.dto.PageRequestDto;
import com.star.robot.enums.ProjectClassEnum;
import com.star.robot.enums.ProjectLevelEnum;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/*
    项目实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "project")
@Builder
public class Project extends PageRequestDto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition = "varchar(255) null COMMENT \"名称\"")
    private String name;

    @Column(columnDefinition = "varchar(255) null COMMENT \"简介\"")
    private String descr;

    @OneToMany(mappedBy = "project")
    private List<ProjectClassEnum> projectClassEnum;

    @Column(columnDefinition = "datetime null COMMENT \"报名开始时间\"")
    private Date baoMingStartTime;

    @Column(columnDefinition = "datetime null COMMENT \"报名结束时间\"")
    private Date baoMingEndTime;

    @Column(columnDefinition = "datetime null COMMENT \"项目结束时间\"")
    private Date projectEndTime;

    @Column(columnDefinition = "bit(1) null COMMENT \"是否可见\"")
    private Boolean status;

    @Column
    private Integer fuDaoCount; //辅导员人数 TODO

    @Column(columnDefinition = "int(10) null COMMENT \"可参赛队员数量\"")
    private Integer teamMemberCount;

    @Column(columnDefinition = "int(10) null COMMENT \"级别\"")
    private ProjectLevelEnum projectLevelEnum;


    @OneToOne
    private DtArea city;
}
