package com.star.robot.entity;

import com.star.robot.enums.TeamClotherSizeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@Table(name = "team_member")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeamMember {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private Integer sex;

    @Column
    private String idCard; //身份证

    @Column
    private TeamClotherSizeEnum size;

    @ManyToOne
    @JoinColumn(name = "team_id",referencedColumnName = "id")
    private Team team;
}
