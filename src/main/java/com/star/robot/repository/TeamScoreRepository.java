package com.star.robot.repository;

import com.star.robot.entity.Team;
import com.star.robot.entity.TeamScore;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamScoreRepository extends JpaSpecificationExecutor, CrudRepository<TeamScore,Long> {
    public TeamScore findByTeam_IdAndLunCi(Long teamId , String lunci);

    @Query(value = "update TeamScore set score=?1 where team_id=?2")
    public void updateScore(Float score , Long teamId);

    @Query(value = "select sum(score) from TeamScore where team_id =?1")
    public Double selectScoreSum(Long teamId);

    public List<TeamScore> findByTeam_Id(Long teamId);
}
