package com.star.robot.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.star.robot.entity.Project;
import com.star.robot.enums.ProjectLevelEnum;

public interface ProjectRepository extends CrudRepository<Project, Long> , JpaSpecificationExecutor<Project> {
    public Project findByName(String name);
    
    public List<Project> findByProjectLevelEnum(ProjectLevelEnum projectLevelEnum);
}
