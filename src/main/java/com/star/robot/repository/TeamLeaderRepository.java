package com.star.robot.repository;

import com.star.robot.entity.TeamMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.star.robot.entity.TeamLeader;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeamLeaderRepository extends JpaSpecificationExecutor, JpaRepository<TeamLeader,Long> {

	public TeamLeader findByPhone(String phone);

	@Query(value = "FROM  TeamLeader WHERE TEAM_ID = ?1")
	public List<TeamLeader> getByTeamId(Long teamId);
}
