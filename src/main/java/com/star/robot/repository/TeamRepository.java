package com.star.robot.repository;

import com.star.robot.entity.Company;
import com.star.robot.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamRepository extends JpaSpecificationExecutor, CrudRepository<Team,Long> {
    public Team getByPhone(String phone);
    public List<Team> getListByPhone(String phone);
    public Team findByTeamNameAndCompany_Id(String teamName , Long companyId);
}
