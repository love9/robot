package com.star.robot.repository;

import com.star.robot.entity.Team;
import com.star.robot.entity.TeamMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.IdClass;
import java.util.List;

public interface TeamMemberRepository extends JpaSpecificationExecutor, JpaRepository<TeamMember,Long> {
	public TeamMember findByIdCard(String idCard);

	@Query(value = "FROM  TeamMember WHERE TEAM_ID = ?1")
	public List<TeamMember> getByTeamId(Long teamId);
}
