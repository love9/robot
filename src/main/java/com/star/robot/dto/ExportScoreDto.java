package com.star.robot.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExportScoreDto {

    private Long teamId;

    private String companyType;

    private String companyName;

    private String teamName;

    private String lunci;
}
