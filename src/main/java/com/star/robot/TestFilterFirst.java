package com.star.robot;

import com.alibaba.fastjson.JSONObject;
import com.star.robot.dto.ResultDto;
import com.star.robot.service.AdminService;
import com.star.robot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Order(1)
//重点
@WebFilter(filterName = "testFilter1", urlPatterns = "/*")
@Component
public class TestFilterFirst implements Filter {
    @Autowired
    private UserService userService;
    @Autowired
    private AdminService adminService;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        if(servletRequest instanceof HttpServletRequest){
            HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
            //白名单　登录页面
//            if(true){
//                filterChain.doFilter(servletRequest,servletResponse);
//                return;
//            }

            if(httpServletRequest.getRequestURI().equals("/login")
                    || httpServletRequest.getRequestURI().equals("/front/login.html")
                    || httpServletRequest.getRequestURI().equals("/user/reg")
                    || httpServletRequest.getRequestURI().equals("/user/login")
                    || httpServletRequest.getRequestURI().equals("/user/adminLogin")
                    || httpServletRequest.getRequestURI().equals("/admin/login")
                    || httpServletRequest.getRequestURI().equals("/**")
                    || httpServletRequest.getRequestURI().equals("/user/login")
//                    || httpServletRequest.getRequestURI().contains("/index")
                    || httpServletRequest.getRequestURI().equals("/logout")
                    || httpServletRequest.getRequestURI().contains("js")
                    || httpServletRequest.getRequestURI().contains("layuiadmin")
                    || httpServletRequest.getRequestURI().contains("views")
                    || httpServletRequest.getRequestURI().contains("download")
                    || httpServletRequest.getRequestURI().contains("upload")
                    || httpServletRequest.getRequestURI().contains("css")
                    || httpServletRequest.getRequestURI().contains("png")
                    || httpServletRequest.getRequestURI().contains("html")

//                    || httpServletRequest.getRequestURI().equals("/projectList")
//                    || httpServletRequest.getRequestURI().contains("/project")
                    //debug
//                    || httpServletRequest.getRequestURI().equals("/companyList")
//
//                    || httpServletRequest.getRequestURI().contains("/company")
//            || httpServletRequest.getRequestURI().equals("/dtArea/getJsonDtArea")
            ){
                filterChain.doFilter(servletRequest,servletResponse);
            }else{
                //web
                if(httpServletRequest.getRequestURI().contains("/front")){
                    if(userService.isUser(httpServletRequest)){
                        filterChain.doFilter(servletRequest,servletResponse);
                    }else{
                        ((HttpServletResponse)servletResponse).sendRedirect("/front/login.html");
//                        return403(servletResponse);
                    }
                }else{
                    if(adminService.isAdmin(httpServletRequest)){
                        filterChain.doFilter(servletRequest,servletResponse);
                    }else{
                        ((HttpServletResponse)servletResponse).sendRedirect("/login");
//                        return403(servletResponse);
                    }
                }
//                String phone = userService.getCurrentUsername(httpServletRequest);
//                if(StringUtils.isEmpty(phone)){
//                    HttpServletResponse httpServletResponse = (HttpServletResponse)servletResponse;
//                    httpServletResponse.addHeader("content-Type","application/json;charset=utf-8");
//                    ResultDto resultDto = ResultDto.builder().success(false).message("未授权").code(403).build();
//                    httpServletResponse.getWriter().println(JSONObject.toJSONString(resultDto));
//                }else{
//                    filterChain.doFilter(servletRequest,servletResponse);
//                }
            }
        }
    }

    private void return403(ServletResponse servletResponse) {

        HttpServletResponse httpServletResponse = (HttpServletResponse)servletResponse;
        httpServletResponse.addHeader("content-Type","application/json;charset=utf-8");
        ResultDto resultDto = ResultDto.builder().success(false).message("未授权").code(403).build();
        try {
            httpServletResponse.getWriter().println(JSONObject.toJSONString(resultDto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {

    }
}