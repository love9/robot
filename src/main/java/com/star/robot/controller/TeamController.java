package com.star.robot.controller;

import com.star.robot.dto.ResultDto;
import com.star.robot.dto.TeamRequestFrontDto;
import com.star.robot.entity.*;
import com.star.robot.repository.TeamRepository;
import com.star.robot.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 前端队伍管理
 */
@RestController
@RequestMapping(value = "/team/front")
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserService userService;

    @PostMapping
    public ResultDto add(@RequestBody TeamRequestFrontDto requestDto, HttpServletRequest request){
        validateRequestParam(requestDto);
        Team team = new Team();
        BeanUtils.copyProperties(requestDto , team);
        //设置队伍关联的用户
        String phone = userService.getCurrentUsername(request);
        team.setPhone(phone);
        teamRepository.save(team);
        return ResultDto.builder().build();
    }

    /**
     *
     * @param request
     * @return
     */
    @GetMapping(value = "/")
    public ResultDto get(HttpServletRequest request){
        String phone = userService.getCurrentUsername(request);
        if(StringUtils.isEmpty(phone)){
            return ResultDto.builder().data(null).build();
        }else{
//            Team team = teamRepository
////                    .getByPhone(phone);
            List<Team> teams= teamRepository.getListByPhone(phone);
            List<Team> reteams=new ArrayList<>();
            for (Team team : teams) {
                //prevent flow
                if(!CollectionUtils.isEmpty(team.getTeamLeaders())){
                    for(TeamLeader teamLeader : team.getTeamLeaders()){
                        teamLeader.setTeam(null);
                    }
                }
                if(!CollectionUtils.isEmpty(team.getTeamMembers())){
                    for(TeamMember teamMember : team.getTeamMembers()){
                        teamMember.setTeam(null);
                    }
                }
                if(!CollectionUtils.isEmpty(team.getTeamScores())){
                    for(TeamScore teamScore : team.getTeamScores()){
                        teamScore.setTeam(null);
                    }
                }

                team.setCompany(null);
                reteams.add(team);
            }

            return  ResultDto.builder().data(reteams).build();
        }
    }




    private void validateRequestParam(TeamRequestFrontDto requestDto) {
        if(requestDto == null || requestDto.getId() != null){
            throw new IllegalArgumentException("添加队伍失败,参数校验异常");
        }

        if(requestDto.getProjectClassEnum() == null){
            throw new IllegalArgumentException("添加队伍失败,组别必填");
        }
        if(StringUtils.isEmpty(requestDto.getTeamName())){
            throw new IllegalArgumentException("添加队伍失败,队名必填");
        }
        if(CollectionUtils.isEmpty(requestDto.getTeamMembers())){
            throw new IllegalArgumentException("添加队伍失败,队员至少一个");
        }
        if(CollectionUtils.isEmpty(requestDto.getTeamLeaders())){
            throw new IllegalArgumentException("添加队伍失败,辅导老师至少一个");
        }
    }

}
