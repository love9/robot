package com.star.robot.controller;

import com.star.robot.dto.PageResultDto;
import com.star.robot.dto.ProjectReqDto;
import com.star.robot.dto.ResultDto;
import com.star.robot.entity.Admin;
import com.star.robot.entity.Company;
import com.star.robot.entity.Project;
import com.star.robot.enums.AdminTypeEnum;
import com.star.robot.enums.ProjectLevelEnum;
import com.star.robot.repository.ProjectRepository;
import com.star.robot.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目管理
 */
@RestController
@RequestMapping("/project")
public class ProjectController {


    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AdminService adminService;

    @PostMapping(value = "/")
    public ResultDto add(@RequestBody Project project, HttpServletRequest httpServletRequest){
        if(project.getProjectLevelEnum() == null){
            Admin admin = adminService.getCurrentAdmin(httpServletRequest);
            if(admin != null){
                if(admin.getAdminTypeEnum() == AdminTypeEnum.PROVINCEADMIN){
                    project.setProjectLevelEnum(ProjectLevelEnum.PROVINCELEVEL);
                }else if(admin.getAdminTypeEnum() == AdminTypeEnum.CITYADMIN){
                    project.setProjectLevelEnum(ProjectLevelEnum.CITYLEVEL);
                }else{
                    return ResultDto.builder().build();
                }
            }
        }
        projectRepository.save(project);
        return ResultDto.builder().build();
    }

    @GetMapping(value = "/")
    public PageResultDto get( ProjectReqDto requestDto) {
        PageResultDto<Company> results = new PageResultDto<>();
        if (requestDto.getPage() != null && requestDto.getLimit() != null) {
            //后端框架分页从0开始 前端框架从1开始
            if (requestDto.getPage() >= 1) {
                requestDto.setPage(requestDto.getPage() - 1);
            }
            Pageable page = new PageRequest(requestDto.getPage(), requestDto.getLimit());
            Page pagedPro = projectRepository.findAll(new Specification<Project>() {
                @Override
                public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                    List<Predicate> predicateList = new ArrayList<>();

                    //修改项目
                    if (requestDto != null && requestDto.getId() != null) {
                        predicateList.add(cb.equal(root.get("id"), requestDto.getId()));
                    }


                    Predicate[] pre = new Predicate[predicateList.size()];
                    cq.where(predicateList.toArray(pre));
                    return cq.getRestriction();
                }
            }, page);

            results.setCount(pagedPro.getTotalElements());
            results.setData(pagedPro.getContent());
        }
        return results;
    }
}
