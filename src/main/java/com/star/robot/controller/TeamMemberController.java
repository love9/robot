package com.star.robot.controller;

import com.star.robot.dto.PageResultDto;
import com.star.robot.dto.ResultDto;
import com.star.robot.dto.TeamMemberQueryDto;
import com.star.robot.dto.TeamMemberRespDto;
import com.star.robot.entity.DtArea;
import com.star.robot.entity.Team;
import com.star.robot.entity.TeamMember;
import com.star.robot.enums.CompanyTypeEnum;
import com.star.robot.enums.ProjectClassEnum;
import com.star.robot.enums.TeamGroupTypeEnum;
import com.star.robot.repository.DtAreaRepositoty;
import com.star.robot.repository.TeamMemberRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 队员管理
 */
@RestController
@RequestMapping(value = "/team/member")
public class TeamMemberController {


    @Autowired
    private TeamMemberRepository teamMemberRepository;


    @Autowired
    private DtAreaRepositoty dtAreaRepositoty;


    @GetMapping(value = "/")
    @ApiOperation(value = "队员查询",notes = "队员查询")
    public PageResultDto get(TeamMemberQueryDto requestDto){
        PageResultDto<TeamMemberRespDto> results = new PageResultDto<>();
        if(requestDto.getPage() != null && requestDto.getLimit() != null) {
            //后端框架分页从0开始 前端框架从1开始
            if (requestDto.getPage() >= 1) {
                requestDto.setPage(requestDto.getPage() - 1);
            }
            Pageable page = new PageRequest(requestDto.getPage(), requestDto.getLimit());

            Page<TeamMember> pageable = teamMemberRepository.findAll(new Specification() {
                @Override
                public Predicate toPredicate(Root root, CriteriaQuery cq, CriteriaBuilder cb) {
                    List<Predicate> conditions = new ArrayList<>();

                    //省
                    if(requestDto.getProvinceId() != null){
                        Predicate provinceCondition = cb.equal(root.get("team").get("company").get("provinceId")  , requestDto.getProvinceId());
                        conditions.add(provinceCondition);
                    }
                    //市
                    if(requestDto.getCityId() != null){
                        Predicate cityCondition = cb.equal(root.get("team").get("company").get("cityId")  , requestDto.getCityId());
                        conditions.add(cityCondition);
                    }
                    //区
                    if(requestDto.getAreaId() != null){
                        Predicate areaCondition = cb.equal(root.get("team").get("company").get("areaId")  , requestDto.getAreaId());
                        conditions.add(areaCondition);
                    }
                    //队员姓名
                    if(!StringUtils.isEmpty(requestDto.getTeamMemberName())){
                        Predicate memberNameCond = cb.like(root.get("name")  , "%"+requestDto.getTeamMemberName()+"%");
                        conditions.add(memberNameCond);
                    }
                    //队员身份证
                    if(!StringUtils.isEmpty(requestDto.getTeamMemberIdCard())){
                        Predicate memberIdCardCond = cb.like(root.get("idCard")  , "%"+requestDto.getTeamMemberName()+"%");
                        conditions.add(memberIdCardCond);
                    }
                    //单位名称
                    if(requestDto.getCompanyName() != null){
                        Predicate companyCond = cb.like(root.get("team").get("company").get("name") , "%"+requestDto.getCompanyName() +"%");
                        conditions.add(companyCond);
                    }
                    //队伍名称
                    if(!StringUtils.isEmpty(requestDto.getTeamName())){
                        Predicate teamNameCond = cb.like(root.get("team").get("teamName") , "%"+requestDto.getTeamName()+"%");
                        conditions.add(teamNameCond);
                    }
                    //单位性质
                    if(requestDto.getCompanyType() !=null){
                        CompanyTypeEnum companyType = null;
                        if(requestDto.getCompanyType().intValue() == 1){
                            companyType =  CompanyTypeEnum.XUEXIAO;
                        }else if(requestDto.getCompanyType().intValue() == 2){
                            companyType =  CompanyTypeEnum.JIGOU;
                        }else{
                            //默认学校
                            companyType =  CompanyTypeEnum.XUEXIAO;
                        }
                        Predicate companyTypeCond = cb.equal(root.get("team").get("company").get("companyType") ,companyType);
                        conditions.add(companyTypeCond);

                    }
                    //成绩 TODO 业务待确认

                    //报名时间 => 队伍　新增时间
                    if(!StringUtils.isEmpty(requestDto.getBaoMingDateStart())
                            && !StringUtils.isEmpty(requestDto.getBaoMingDateEnd())){
                        Predicate baoMingDateCond = cb.between(root.get("team").get("createTime") ,requestDto.getBaoMingDateStart() , requestDto.getBaoMingDateEnd());
                        conditions.add(baoMingDateCond);
                    }
                    //参赛类别
                    //大类
//                    if(requestDto.getClass1Id() != null){
//                        Predicate class1IdCond = cb.equal(root.get("team").get("class1Id") ,requestDto.getClass1Id());
//                        conditions.add(class1IdCond);
//                    }
//                    //小类
//                    if(requestDto.getClass2Id() != null){
//                        Predicate class2IdCond = cb.equal(root.get("team").get("class2Id") ,requestDto.getClass2Id());
//                        conditions.add(class2IdCond);
//                    }
                    //组别
                    if(requestDto.getGroupType() != null){
                        ProjectClassEnum projectClassEnum = null;
                        if(requestDto.getGroupType() == 1){
                            projectClassEnum = ProjectClassEnum.XIAOXUE;
                        }else if(requestDto.getGroupType() == 2){
                            projectClassEnum = ProjectClassEnum.CHUZHONG;
                        }else if(requestDto.getGroupType() == 3){
                            projectClassEnum = ProjectClassEnum.GAOZHONG;
                        }else{
                            //默认小学组
                            projectClassEnum = ProjectClassEnum.XIAOXUE;
                        }
                        Predicate groupTypeCond = cb.equal(root.get("team").get("groupType") ,projectClassEnum);
                        conditions.add(groupTypeCond);
                    }
                    if(requestDto.getSizeEnum() != null){
                        Predicate sizeCond = cb.equal(root.get("size") ,requestDto.getSizeEnum());
                        conditions.add(sizeCond);
                    }

                    Predicate[] pre = new Predicate[conditions.size()];

                    cq.where(conditions.toArray(pre));
                    return cq.getRestriction();
                }
            } , page);



            List<TeamMemberRespDto> teamMemberRespDtos = new ArrayList<>();
            List<TeamMember> teamMembers = pageable.getContent();
            if(!CollectionUtils.isEmpty(teamMembers)){
                for(TeamMember teamMember : teamMembers){
                    String companyType = teamMember.getTeam().getCompany().getCompanyType().getName();
                    String companyName = teamMember.getTeam().getCompany().getName();
                    String teamName = teamMember.getTeam().getTeamName();
                    String projectType = teamMember.getTeam().getProjectClassEnum().getName();
                    String projectName = teamMember.getTeam().getProject().getName();

                    String areaName = null;
                    String cityName = null;
                    if(teamMember.getTeam().getCompany().getAreaId() != null ){

                        DtArea area = dtAreaRepositoty.findById(Long.valueOf(teamMember.getTeam().getCompany().getAreaId())).get();
                        DtArea city = dtAreaRepositoty.findById(Long.valueOf(teamMember.getTeam().getCompany().getCityId())).get();
                        areaName = area != null ? area.getAreaName() : null;
                        cityName = city != null ? city.getAreaName() : null;
                    }





                    TeamMemberRespDto teamTeamMemberRespDto = TeamMemberRespDto.builder()
                        .companyType(companyType)
                            .companyName(companyName)
                            .teamName(teamName)
                            .projectType(projectType)
                            .groupType(projectName)
                            .name(teamMember.getName())
                            .sex(teamMember.getSex() == 1 ?"男" :"女")
                            .idCard(teamMember.getIdCard())
                            .size(teamMember.getSize().getName())
                            .school(teamMember.getTeam().getCompany().getName())
                            .areaName(areaName)
                            .cityName(cityName)
                            .build();
                    teamMemberRespDtos.add(teamTeamMemberRespDto);
                }

            }

            Page pagedTeamMembers = new PageImpl(teamMemberRespDtos , page ,pageable.getTotalElements() );


            results.setData(pagedTeamMembers.getContent());
            results.setCount(pagedTeamMembers.getTotalElements());
            return results;
        }
        return PageResultDto.builder().data(null).build();
    }

    @GetMapping(value = "/teamId")
    @ApiOperation(value = "队伍队员查询",notes = "队伍队员查询")
    public ResultDto getTeamMember(TeamMemberQueryDto teamMemberQueryDto) {

        if(teamMemberQueryDto.getTeamId() != null){
            List<TeamMember> teamMembers =  teamMemberRepository.getByTeamId(teamMemberQueryDto.getTeamId());

            if(!CollectionUtils.isEmpty(teamMembers)){
                for(TeamMember teamMember : teamMembers){
                    teamMember.setTeam(null);
                }
            }
            return ResultDto.builder().data(teamMembers).build();
        }
        return ResultDto.builder().data(null).build();

    }

}
