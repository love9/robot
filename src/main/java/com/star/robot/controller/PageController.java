package com.star.robot.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.star.robot.constant.Constant;
import com.star.robot.entity.Admin;
import com.star.robot.entity.Project;
import com.star.robot.enums.ProjectLevelEnum;
import com.star.robot.repository.ProjectRepository;

@Controller
public class PageController {
	
	@Autowired
	private ProjectRepository projectRepository;
	
	/**
	 * 登陆页
	 * @param model
	 * @return
	 */
	@RequestMapping("/login")
	public String login(Map<String, Object> model) {
		return "/home/login";
	}
	
	/**
	 * 登出页
	 * @param model
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, Map<String, Object> model) {
		request.getSession().invalidate();
		return "/home/login";
	}
	
	/**
	 * 主页
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/", "/index" })
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		//获取session  		
        Admin admin = (Admin) request.getSession().getAttribute(Constant.CURRENTADMIN);
        if(admin != null){
			mav.addObject("name", admin.getUsername());
		}else{
			mav.addObject("name", "管理员");
		}
		mav.setViewName("/home/index");
		return mav;
	}
	
	/**
	 * 首页
	 * @param model
	 * @return
	 */
	@RequestMapping("/console")
	public String console(Map<String, Object> model) {
		return "/home/console";
	}

	/**
	 * 管理员页
	 * @return
	 */
	@RequestMapping("/teamManagement")
	public ModelAndView teamManagement() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/team/teamManagement");
		return mav;
	}
	
	/**
	 * 添加管理员页
	 * @return
	 */
	@RequestMapping("/addTeamInfo")
	public ModelAndView addTeamInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/team/addTeamInfo");
		return mav;
	}
	
	/**
	 * 单位页
	 * @return
	 */
	@RequestMapping("/companyList")
	public ModelAndView companyList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/company/companyList");
		return mav;
	}
	
	/**
	 * 添加单位页
	 * @return
	 */
	@RequestMapping("/addCompanyInfo")
	public ModelAndView addCompanyInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/company/addCompanyInfo");
		return mav;
	}
	
	/**
	 * 项目页
	 * @return
	 */
	@RequestMapping("/projectList")
	public ModelAndView projectList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/projectList");
		return mav;
	}
	
	/**
	 * 添加省级项目
	 * @return
	 */
	@RequestMapping("/addProvinceProjectInfo")
	public ModelAndView addProvinceProjectInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/addProvinceProjectInfo");
		return mav;
	}
	
	/**
	 * 添加市级项目
	 * @return
	 */
	@RequestMapping("/addCityProjectInfo")
	public ModelAndView addCityProjectInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/project/addCityProjectInfo");
		List<Project> list = projectRepository.findByProjectLevelEnum(ProjectLevelEnum.PROVINCELEVEL);
		mav.addObject("projectList", list);
		return mav;
	}
	
	/**
	 * 缴费管理页
	 * @return
	 */
	@RequestMapping("/payInfoList")
	public ModelAndView payInfoList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/info/payInfoList");
		return mav;
	}
	
	/**
	 * 报名项目页
	 * @return
	 */
	@RequestMapping("/signUpInfoList")
	public ModelAndView signUpInfoList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/info/signUpInfoList");
		return mav;
	}
	
	/**
	 * 队员管理页
	 * @return
	 */
	@RequestMapping("/teamManage")
	public ModelAndView teamManage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/teamManage");
		return mav;
	}
	
	/**
	 * 队伍管理页
	 * @return
	 */
	@RequestMapping("/ranksManage")
	public ModelAndView ranksManage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/ranksManage");
		return mav;
	}
	
	/**
	 * 成绩管理页（省）
	 * @return
	 */
	@RequestMapping("/scoreProvinceManage")
	public ModelAndView scoreProvinceManage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/scoreProvinceManage");
		return mav;
	}
	
	/**
	 * 成绩管理页（市）
	 * @return
	 */
	@RequestMapping("/scoreCityManage")
	public ModelAndView scoreCityManage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/scoreCityManage");
		return mav;
	}
	
	/**
	 * 导出页
	 * @return
	 */
	@RequestMapping("/listform")
	public ModelAndView listform() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/listform");
		return mav;
	}
	
	/**
	 * 导出页
	 * @return
	 */
	@RequestMapping("/listformT")
	public ModelAndView listformT() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/listform2");
		return mav;
	}
	
	/**
	 * 上传页
	 * @return
	 */
	@RequestMapping("/uploadFile")
	public ModelAndView uploadFile() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/manage/uploadFile");
		return mav;
	}
}
