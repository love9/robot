package com.star.robot.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.star.robot.entity.*;
import com.star.robot.enums.TeamGroupTypeEnum;
import com.star.robot.repository.*;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.star.robot.enums.CompanyTypeEnum;
import com.star.robot.enums.ProjectClassEnum;
import com.star.robot.enums.TeamClotherSizeEnum;
import com.star.robot.util.StringUtil;

@Service
public class ScoreService {
	@Autowired
    private CompanyRepository companyRepository;
	private HSSFWorkbook workbook;
	
    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TeamMemberRepository teamMemberRepository;
    @Autowired
    private TeamLeaderRepository teamLeaderRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
	private TeamScoreRepository teamScoreRepository;
    @Autowired
	private UserRepositoty userRepositoty;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String batchImport(String fileName, MultipartFile file) throws Exception {
		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			throw new Exception("上传文件格式不正确");
		}
		//List<Company> companyList = new ArrayList<>();
		try {
			InputStream is = file.getInputStream();
			workbook = new HSSFWorkbook(new POIFSFileSystem(is));
			// 有多少个sheet
			int sheets = workbook.getNumberOfSheets();

			for (int i = 0; i < sheets; i++) {
				HSSFSheet sheet = workbook.getSheetAt(i);
				// 获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
				Company company = null;
				Team team = null;
				Project project = null;
				// 遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
					company = new Company();
					team = new Team();
					project = new Project();
					company = new Company();
					// 获得第 j 行
					HSSFRow row = sheet.getRow(j);
					//单位bean
					System.out.println("row=" + row);
					//
					company.setCompanyType("学校".equals(StringUtil.getCellValue(row.getCell(0)))?CompanyTypeEnum.XUEXIAO:CompanyTypeEnum.JIGOU);
					//
					company.setName(StringUtil.getCellValue(row.getCell(1)));
					//companyList.add(company);
					//
					team.setTeamName(StringUtil.getCellValue(row.getCell(2)));					
					//
					project.setName(StringUtil.getCellValue(row.getCell(3)));
					Project oldProject = projectRepository.findByName(project.getName());
					if(oldProject != null){
						team.setProject(oldProject);
					}else{
						projectRepository.save(project);
						team.setProject(project);
					}
					//
					if ("小学组".equals(StringUtil.getCellValue(row.getCell(4)))) {
						team.setProjectClassEnum(ProjectClassEnum.XIAOXUE);
					}else if("初中组".equals(StringUtil.getCellValue(row.getCell(4)))) {
						team.setProjectClassEnum(ProjectClassEnum.CHUZHONG);
					}else {
						team.setProjectClassEnum(ProjectClassEnum.GAOZHONG);
					}
					//
					List<TeamLeader> teamLeaders = new ArrayList<TeamLeader>();
					TeamLeader teamLeader = new TeamLeader();
					teamLeader.setName(StringUtil.getCellValue(row.getCell(7)).split("、")[0]);
					if (StringUtil.getCellValue(row.getCell(7)).split("、").length>1) {
						TeamLeader teamLeader2 = new TeamLeader();
						teamLeader2.setName(StringUtil.getCellValue(row.getCell(7)).split("、")[1]);
						teamLeaders.add(teamLeader2);
						teamLeader2.setPhone(StringUtil.getCellValue(row.getCell(9)));
					}					
					//
					teamLeader.setPhone(StringUtil.getCellValue(row.getCell(8)));
					teamLeaders.add(teamLeader);
					team.setTeamLeaders(teamLeaders);
					
					//队员
					List<TeamMember> teamMembers = new ArrayList<>();
					TeamMember teamMember1 = TeamMember.builder().name(StringUtil.getCellValue(row.getCell(10))).idCard(StringUtil.getCellValue(row.getCell(11))).build();
					TeamMember teamMember2 = TeamMember.builder().name(StringUtil.getCellValue(row.getCell(13))).idCard(StringUtil.getCellValue(row.getCell(14))).build();
					TeamMember teamMember3 = TeamMember.builder().name(StringUtil.getCellValue(row.getCell(16))).idCard(StringUtil.getCellValue(row.getCell(17))).build();
					TeamMember teamMember4 = TeamMember.builder().name(StringUtil.getCellValue(row.getCell(19))).idCard(StringUtil.getCellValue(row.getCell(20))).build();
					teamMember1.setSize(getClothesSize(StringUtil.getCellValue(row.getCell(12))));
					teamMember2.setSize(getClothesSize(StringUtil.getCellValue(row.getCell(15))));
					teamMember3.setSize(getClothesSize(StringUtil.getCellValue(row.getCell(18))));
					teamMember4.setSize(getClothesSize(StringUtil.getCellValue(row.getCell(21))));
					//默认男
					teamMember1.setSex(1);
					teamMember2.setSex(1);
					teamMember3.setSex(1);
					teamMember4.setSex(1);
					teamMembers.add(teamMember1);
					teamMembers.add(teamMember2);
					teamMembers.add(teamMember3);
					teamMembers.add(teamMember4);
					team.setTeamMembers(teamMembers);
					
					//fapiao
					team.setFapiaoStatus(StringUtil.getCellValue(row.getCell(22)).equals("是") ? true : false);
					
					//单位全称
					company.setZhiZhao(StringUtil.getCellValue(row.getCell(23)));
					
					//纳税人识别号
					company.setInvoiceTaxNo(StringUtil.getCellValue(row.getCell(24)));
					
					//
					Company oldCompany = companyRepository.findByName(company.getName());
					if(oldCompany == null) {
						this.companyRepository.save(company);
					}else {
						company.setId(oldCompany.getId());
						this.companyRepository.save(company);
					}
					
					team.setCompany(company);

					Team dbTeam = teamRepository.findByTeamNameAndCompany_Id(team.getTeamName() , company.getId());
					if(dbTeam == null){
						this.teamRepository.save(team);
					}else{
						team.setId(dbTeam.getId());
						this.teamRepository.save(team);
					}


					if(!CollectionUtils.isEmpty(teamMembers)) {

						for(TeamMember teamMember : teamMembers) {


							TeamMember oldMember = this.teamMemberRepository.findByIdCard(teamMember.getIdCard());
							teamMember.setTeam(team);
							if(oldMember == null) {
								this.teamMemberRepository.save(teamMember);
							}else {
								teamMember.setId(oldMember.getId());
								this.teamMemberRepository.save(teamMember);
							}
						}
						
					}
					
					if(!CollectionUtils.isEmpty(teamLeaders)) {
						for(TeamLeader tempLeader : teamLeaders) {
							int x = 0;
							if(x == 0){
								addFrontUser(teamLeader,company);
							}
							x++;
							tempLeader.setTeam(team);
							TeamLeader oldlLeader = this.teamLeaderRepository.findByPhone(tempLeader.getPhone());
							if(oldlLeader == null) {
								this.teamLeaderRepository.save(tempLeader);
							}else {
								tempLeader.setId(oldlLeader.getId());
								this.teamLeaderRepository.save(tempLeader);
							}
						}
						
					}
					
					
					
				}
			}
			//companyRepository.saveAll(companyList);
		} catch (Exception e) {
			e.printStackTrace();
			return "导入数据格式有误，请检查上传文件";
		}
		return "导入数据成功";
	}
	

	private void addFrontUser(TeamLeader teamLeader,Company company) {
		User oldUser = userRepositoty.findByPhone(teamLeader.getPhone());
		if(oldUser == null){
			userRepositoty.save(User.builder()
					.account(teamLeader.getPhone())
					.company(company)
					.phone(teamLeader.getName())
					.passwd(getPassword(teamLeader.getPhone()))
					.build());
		}
	}

	private String getPassword(String name) {
		return name.substring(name.length() - 6 , name.length());
	}


	public TeamClotherSizeEnum getClothesSize(String sizeStr) {
		if (sizeStr.startsWith("S")) {
			return TeamClotherSizeEnum.S;
		}else if(sizeStr.startsWith("M")) {
			return TeamClotherSizeEnum.M;
		}else if(sizeStr.startsWith("L")){
			return TeamClotherSizeEnum.L;
		}else if(sizeStr.startsWith("XL")){
			return TeamClotherSizeEnum.XL;
		}else if(sizeStr.startsWith("XXXL")){
			return TeamClotherSizeEnum.XXXL;
		}
		return TeamClotherSizeEnum.XXXL;
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
    public Object importScore(String fileName, MultipartFile file)throws Exception {
		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			throw new Exception("上传文件格式不正确");
		}
		//List<Company> companyList = new ArrayList<>();
		try {
			InputStream is = file.getInputStream();
			workbook = new HSSFWorkbook(new POIFSFileSystem(is));
			// 有多少个sheet
			int sheets = workbook.getNumberOfSheets();


			for (int i = 0; i < sheets; i++) {
				HSSFSheet sheet = workbook.getSheetAt(i);
				// 获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
				Company company = null;
				Team team = null;
				Project project = null;
				// 遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
					company = new Company();
					team = new Team();
					project = new Project();
					company = new Company();
					// 获得第 j 行
					HSSFRow row = sheet.getRow(j+1);
					//单位bean
					System.out.println("row=" + row);
					//队伍id
					Long teamId = Long.valueOf(StringUtil.getCellValue(row.getCell(0)));
					//轮次
					String lunci = StringUtil.getCellValue(row.getCell(4));
					//成绩
					Float score = Float.valueOf(StringUtil.getCellValue(row.getCell(5)));

					Team oldTeam =teamRepository.findById(teamId).get();
					if(oldTeam == null ){
						return "false";
					}

					TeamScore teamScore = teamScoreRepository.findByTeam_IdAndLunCi(teamId , lunci);
					TeamScore newScore = TeamScore.builder()
							.team(oldTeam)
							.lunCi(lunci)
							.score(score)
							.build();
					if(teamScore != null){
						teamScoreRepository.updateScore(score, teamId);
					}else{
						teamScoreRepository.save(newScore);
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "导入数据格式有误，请检查上传文件";
		}
		return "导入数据成功";
    }
}
