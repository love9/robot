package com.star.robot.service;

import com.star.robot.constant.Constant;
import com.star.robot.entity.Admin;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class AdminService {

    public String getCurrentUsername(HttpServletRequest request){
        Admin currentAdmin = getCurrentAdmin(request);
        return currentAdmin  == null ? null : currentAdmin.getUsername().toString();
    }

    public Admin getCurrentAdmin(HttpServletRequest request){
        Object currentAdmin = request.getSession().getAttribute(Constant.CURRENTADMIN);

        return (Admin)currentAdmin;
    }

    public Boolean isAdmin(HttpServletRequest request){
        return getCurrentAdmin(request) != null;
    }
}
