package com.star.robot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class RoleService {

    @Autowired
    private UserService userService;

    @Autowired
    private AdminService adminService;

    public Boolean isAdmin(HttpServletRequest request){
        return adminService.isAdmin(request);
    }
}
