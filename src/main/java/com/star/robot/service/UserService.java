package com.star.robot.service;

import com.star.robot.constant.Constant;
import com.star.robot.entity.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserService {
    /**
     *
     * @return　手机号
     */
    public String getCurrentUsername(HttpServletRequest request){
        User currentUser = getCurrentUser(request);
        return currentUser  == null ? null : currentUser.getPhone().toString();
    }

    public User getCurrentUser(HttpServletRequest request){
        Object currentUser = request.getSession().getAttribute(Constant.CURRENTUSER);
        if(currentUser != null){
            return (User)currentUser;
        }
        return null;
    }

    public Boolean isUser(HttpServletRequest request){
        return  getCurrentUser(request) != null;
    }
}
