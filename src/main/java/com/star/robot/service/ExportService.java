package com.star.robot.service;

import com.star.robot.constant.Constant;
import com.star.robot.dto.ExportScoreDto;
import com.star.robot.dto.ResultDto;
import com.star.robot.entity.Company;
import com.star.robot.entity.Team;
import com.star.robot.enums.CompanyTypeEnum;
import com.star.robot.repository.CompanyRepository;
import com.star.robot.util.ExportExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExportService {

    @Autowired
    private CompanyRepository companyRepository;
    private final String sheetName = "成绩导出表";

    private final String titleName = sheetName;

    /**
     * 生成xls
     * @return
     */
    public String exportScore(){
        String [] headers = new String[]{ "队伍ID" , "单位/机构" , "单位名称" , "队伍名称" , "轮次" , "成绩"};

        List<ExportScoreDto> exportScoreDtos = new ArrayList<>();
        List<Company> companies = companyRepository.findAll();
        if(!CollectionUtils.isEmpty(companies)) {
            for (Company company : companies) {

                String companyTypeStr = getCompanyTypeStr(company.getCompanyType());
                List<Team> teams = company.getTeams();
                if (!CollectionUtils.isEmpty(teams)) {
                    for (Team team : teams) {
                        ExportScoreDto exportScoreDto = ExportScoreDto.builder()
                                .teamId(team.getId())
                                .companyType(companyTypeStr)
                                .companyName(company.getZhiZhao())
                                .teamName(team.getTeamName())
                                .build();
                        exportScoreDtos.add(exportScoreDto);
                    }
                }

            }
        }

        String fileName = System.currentTimeMillis()+".xls";
        ExportExcel.exportExcel(sheetName , titleName , headers  , exportScoreDtos , Constant.downloadPath+"/"+fileName,  null);
        return fileName;
    }
    public String getCompanyTypeStr(CompanyTypeEnum companyTypeEnum){
        return companyTypeEnum.getName();
    }
}
