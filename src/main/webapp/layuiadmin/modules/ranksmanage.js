/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table,
    n = layui.form;   
    i.render({
        elem: "#manage",
        url: "/team/backend/backend",
        page: true,
        cols: [[{
            type: "checkbox"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "areaId",
            title: "所在地区"
        },
        {
            field: "companyType",
            title: "学校/机构"
        },
        {
            field: "companyName",
            title: "单位名称"
        },
        {
            field: "teamName",
            title: "队伍名称"
        },
        {
            field: "teamMemberName",
            title: "队员名字"
        },
        {
            field: "teamMemberIdCard",
            title: "身份证号"
        },
        {
            field: "teamType",
            title: "组别"
        },
        {
            field: "teamScoreSum",
            title: "总成绩(每轮成绩之和)"
        },
        {
            field: "leaderCount",
            title: "查看辅导老师(人数)",
            templet: function(res){
            	return '<a style="color:blue;" url="">查看</a>('+res.leaderCount+')';
            }
        },
        {
            field: "memberCount",
            title: "查看队员(人数)",
            templet: function(res){
            	return '<a style="color:blue;"  url="">查看</a>('+res.memberCount+')';
            }
        },
        {
            field: "id",
            title: "操作",
            templet: function(res){
            	return '<a style="color:blue;"  url="">修改</a>';
            }
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("ranksmanage", {})
});