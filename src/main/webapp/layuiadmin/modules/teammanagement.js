/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#adminTable",
        url: "admin/query",
        page: true,
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
            field: "id",
            title: "序号",
            type:"numbers"
        },
        {
            field: "id",
            title: "所属地区",
            templet: function(res){
                var strName = res.provinceName + res.cityName + res.areaName ;
                return '<span style="color:blue;">'+strName+'</span>';
            }
        },
        {
            field: "username",
            title: "用户名"
        },
        {
            field: "passwd",
            title: "密码"
        },
        {
            field: "status",
            title: "有效标志",
            templet: function(res){
                if(res.status == true){
                    return '有效'
                }else if(res.status == false){
                    return '无效'
                }

            }
        },
        {
            field: "adminTypeEnum",
            title: "权限",
            templet: function(res){
                if(res.adminTypeEnum == "PROVINCEADMIN"){
                    return '省级'
                }else if(res.adminTypeEnum == "CITYADMIN"){
                    return '市级'
                }

            }
        },
        {
        	field: "修改",
            title: "操作",
            width: 150,
            align: "center",
            fixed: "right",
            toolbar: "#table-useradmin-admin"
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("teammanagement", {})
});

