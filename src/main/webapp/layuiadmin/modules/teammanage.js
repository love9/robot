/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table,
    n = layui.form;   
    i.render({
        elem: "#manage",
        url: "team/member/",
        page: true,
        cols: [[{
            type: "checkbox"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "name",
            title: "队员姓名"
        },
        {
            field: "id_card",
            title: "所在地区"
        },
        {
            field: "companyType",
            title: "学校/机构"
        },
        {
            field: "companyName",
            title: "单位简称"
        },
        {
            field: "teamName",
            title: "队伍名称"
        },
        {
            field: "groupType",
            title: "参赛类别"
        },
        {
            field: "projectType",
            title: "组别"
        },
        {
        	field: "sex",
            title: "性别"
        },
        {
            field: "idCard",
            title: "身份证号"
        },
        {
            field: "size",
            title: "队服尺码"
        },
        {
            field: "school",
            title: "所属学校"
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("teammanage", {})
});