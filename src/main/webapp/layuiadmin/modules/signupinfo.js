/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#manage",
        url: "xxx",
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "id",
            title: "单位名称"
        },
        {
            field: "id_card",
            title: "所在地区"
        },
        {
            field: "name",
            title: "学校/机构"
        },
        {
            field: "school",
            title: "队名"
        },
        {
            field: "sex",
            title: "创建时间"
        },
        {
            field: "sex",
            title: "缴费状态"
        },
        {
            field: "查看",
            title: "队伍数"
        },
        {
            field: "sex",
            title: "参赛队员总数"
        },
        {
            field: "修改",
            title: "操作"
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("signupinfo", {})
});