/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;
    i.render({
        elem: "#projectTable",
        url: "/project/",
        page: true,
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
        	field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "name",
            title: "大赛项目"
        },
        {
            field: "descr",
            title: "项目简介"
        },
        {
            field: "teamMemberCount",
            title: "队员最多可添加数量"
        },
        {
            field: "projectLevelEnum",
            title: "级别"
        },
        {
            field: "baoMingStartTime",
            title: "报名开始时间"
        },
        {
            field: "baoMingEndTime",
            title: "报名结束时间"
        },
        {
            field: "status",
            title: "是否可见"
        },
        {
            field: "ProjectClassEnum",
            title: "组别"
        },
        {
            field: "修改",
            title: "操作"
        }]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("project", {})
});