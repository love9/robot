package com.star.robot.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.star.robot.entity.Company;
import com.star.robot.entity.Project;
import com.star.robot.entity.Team;
import com.star.robot.entity.TeamLeader;
import com.star.robot.entity.TeamMember;
import com.star.robot.enums.CompanyTypeEnum;
import com.star.robot.enums.TeamClotherSizeEnum;
import com.star.robot.repository.CompanyRepository;
import com.star.robot.repository.ProjectRepository;
import com.star.robot.repository.TeamLeaderRepository;
import com.star.robot.repository.TeamMemberRepository;
import com.star.robot.repository.TeamRepository;
@RunWith(SpringRunner.class)
@SpringBootTest
public class AsciiTests {

    @Test
    public void letter2Ascii(){
//        byte a = 97;
//
//        for(byte i = 0 ; i <=127;i++)
//        System.out.println(MD5Util.ascii(i));
//        Team team = new Team();
//        team.setId(1L);
//
//        //设置队伍关联的用户
//        String phone = "123";
//        team.setPhone(phone);
//        teamRepository.save(team);

    }
    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TeamMemberRepository teamMemberRepository;
    @Autowired
    private TeamLeaderRepository teamLeaderRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Test
    @Rollback
    public void importTeam(){
        Company company = new Company();
        company.setCompanyType(CompanyTypeEnum.XUEXIAO);
        company.setName("濮阳县第一中学");
        company.setZhiZhao("濮阳县华为");

        companyRepository.save(company);
        TeamMember teamMember = TeamMember.builder().name("王铭哲").idCard("320124199108131216").size(TeamClotherSizeEnum.S).build();

        TeamMember teamMember2 = TeamMember.builder().name("李晴天").idCard("320124199108131215").size(TeamClotherSizeEnum.M).build();

        List<TeamMember> teamMemberList = new ArrayList<>();
        teamMemberList.add(teamMember);
        teamMemberList.add(teamMember2);

        List<TeamLeader> teamLeaders = new ArrayList<>();

        TeamLeader teamLeader = TeamLeader.builder().name("杨磊").phone("123").build();
        teamLeaders.add(teamLeader);

        teamLeaderRepository.saveAll(teamLeaders);

        teamMemberRepository.saveAll(teamMemberList);
        Project project = Project.builder().name("WRO常规赛(A类)").build();
        projectRepository.save(project);
        Team team = Team.builder().phone("123").company(company).teamLeaders(teamLeaders).teamMembers(teamMemberList).teamName("华龙区八中一对").project(project).build();
        teamRepository.save(team);
        for(TeamLeader leader : teamLeaders){
            leader.setTeam(team);
            teamLeaderRepository.save(leader);
        }

        for(TeamMember member : teamMemberList){
            member.setTeam(team);
            teamMemberRepository.save(member);
        }

    }
}
